import { Injectable } from '@angular/core';
import { DisplayVo } from './display-vo';
import { SearchVo } from './search-vo';

@Injectable()
export class CompanyService{

  getDisplayVo() : DisplayVo {
    let disVo = new DisplayVo();
    disVo.companies = ['Tesla', 'Ford', 'Benz'];
    disVo.status =  ['Active', 'Pending', 'Closed'];
    disVo.vendors = ['Tesco Corp', 'Walmart Ltd', 'MingoSys', 'Jarvin Land', 'Wikla', 'ShiShine'];
    disVo.defaultCompany = 'Tesla';
    disVo.defaultStatus =  'Active';
    return disVo;
  }
}
