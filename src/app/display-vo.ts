export class DisplayVo {
  companies: string[];
  vendors: string[];
  status: string[];
  defaultCompany: string;
  defaultStatus: string;
}
