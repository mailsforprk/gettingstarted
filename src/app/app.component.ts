import {Component, OnInit} from '@angular/core';
import { SearchVo } from './search-vo';
import { CompanyService } from './company.service';
import {DisplayVo} from "./display-vo";

@Component({
  selector: 'my-app',
  providers: [CompanyService],
  template: `

<h1>Vendor Agre</h1>
<div class="container">
<div class="row" >
<label  class="col-sm-1">Vendor:</label> <br> <br>
<div style="float: left">
 <input auto-complete [(ngModel)]="searchVo.vendor" [source]="displayVo.vendors" />
 </div>
 </div>
<div class="row">
<div class="col-sm-2">
<label  >Company:</label> 
<select class="form-control" (change)="setSelectedCompany($event.target.value)" >
<option value="{{displayVo.defaultCompany}}">{{displayVo.defaultCompany}}</option>
<option *ngFor="let company of displayVo.companies" > {{company}}
</option>
</select>
</div>
<div class="col-sm-2">
<label >Status:</label> 
<select class="form-control" (change)="setSelectedStatus($event.target.value)" >
<option value="{{displayVo.defaultStatus}}">{{displayVo.defaultStatus}}</option>
<option *ngFor="let sta of displayVo.status" > {{sta}}
</option>
</select>
</div>
</div>
<div class="row">
  <div class="col-sm-2">

<button class="btn btn-primary" (click)="getSearchResult()" >Search</button>
</div>
</div>
</div>
`,
})
export class AppComponent implements OnInit {
  searchVo =   new SearchVo();
  displayVo : DisplayVo;

  constructor(private companyService : CompanyService){};

  setSelectedCompany(company: string): void {
    this.searchVo.company = company;
    console.log(company);
  };

  setSelectedStatus(status: string): void {
    this.searchVo.status = status;
    console.log(status);
  };

  ngOnInit(): void {
    this.displayVo = this.companyService.getDisplayVo();
    this.searchVo.status = this.displayVo.defaultStatus;
    this.searchVo.company = this.displayVo.defaultCompany;
    this.displayVo.companies = this.filterDefaultValue(this.displayVo.defaultCompany, this.displayVo.companies);
    this.displayVo.status = this.filterDefaultValue(this.displayVo.defaultStatus, this.displayVo.status);

    console.log(this.displayVo);
  };

  filterDefaultValue(defValue: string, arrayOfVal: string[]): string[] {
    arrayOfVal = arrayOfVal.filter(item => item !== defValue);
    console.log(arrayOfVal);
    return arrayOfVal;
  }

  getSearchResult(): void {
console.log(this.searchVo);
  }


}
