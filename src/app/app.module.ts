import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';
import { AppComponent }  from './app.component';

@NgModule({
  imports:      [ BrowserModule, Ng2AutoCompleteModule ],
  declarations: [ AppComponent ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
